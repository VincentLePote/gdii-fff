﻿using System.Collections;
using UnityEngine;

public class DoorAntScript : MonoBehaviour
{
    public Transform marker1;
    public Transform marker2;
    public float idleTime = 3.0f;
    public float speed = 1.0f;
    private float startTime;
    private float journeyLength;
    public bool moving = false;
    
    IEnumerator Patroll() {
        yield return new WaitForSeconds(idleTime);
        StartMoving();
    }

    void Start() {
        StartCoroutine(Patroll());
    }

    void StartMoving()
    {
        startTime = Time.time;
        journeyLength = Vector3.Distance(marker1.position, marker2.position);
        moving = true;
    }

    void StopMoving() {
        moving = false;
        Transform tmp = marker2;
        marker2 = marker1;
        marker1 = tmp;
    }

    // Update is called once per frame
    void Update()
    {
        if (moving) {
            // Distance moved equals elapsed time times speed..
            float distCovered = (Time.time - startTime) * speed;
            // Fraction of journey completed equals current distance divided by total distance.
            float fractionOfJourney = distCovered / journeyLength;
            // Set our position as a fraction of the distance between the markers.
            transform.position = Vector3.Lerp(marker1.position, marker2.position, fractionOfJourney);
            if (transform.position == marker2.position) {
                moving = false;
                StopMoving();
                StartCoroutine(Patroll());
            }
        }
    }
}
