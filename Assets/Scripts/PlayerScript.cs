﻿using UnityEngine;

public class PlayerScript : MonoBehaviour
{ 
    public float speed = 6.0f;
    public float zoomedSpeed = 6.0f;
    public float normalSpeed = 2.0f;

    private Vector3 moveDirection = Vector3.zero;
    public Transform resetPoint;

    public static GameObject reference;
    private CharacterController controller;
    public CameraScript camScriptRef;
    public EnnemyManager ennemyManagerRef;
    private Quaternion facing;

    void Awake () {
        reference = this.gameObject;
        //ennemyManagerRef = EnnemyManager.reference.GetComponent<EnnemyManager>();
        controller = GetComponent<CharacterController>();
        //camScriptRef = CameraScript.reference.GetComponent<CameraScript>();
    }

    void OnTriggerEnter(Collider coll) {
        Debug.Log(coll.gameObject.layer);
        switch (coll.gameObject.layer)
        {
            case 11:
                Debug.Log("touched");
                controller.enabled = false;
                transform.position = resetPoint.position;
                controller.enabled = true;
                ennemyManagerRef.ResetAllEnnemies();
                break;
            case 12:
                Debug.Log("oufed");
                resetPoint = coll.transform;
                Debug.Log(resetPoint.position);
                break;
            case 14:
                Debug.Log("trigger");
                coll.GetComponent<TriggerScript>().TriggerEnnemies();
                break;
            default:
                break;
        }  
    }
    void Update() {
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            //transform.rotation = Quaternion.LookRotation(moveDirection, Vector3.up);
            moveDirection *= speed;
            controller.Move(moveDirection * Time.deltaTime);
        }
        if (Input.GetButton("Jump")) {
            speed = normalSpeed;
            camScriptRef.ZoomOut();
        }
        else {
            camScriptRef.ZoomIn();
            speed = zoomedSpeed;
        }
     }
 }