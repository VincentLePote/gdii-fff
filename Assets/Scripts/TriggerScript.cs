﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerScript : MonoBehaviour
{
    [SerializeField]
    public EnnemyScript[] ennemies;

    public void TriggerEnnemies() {
        foreach (EnnemyScript e in ennemies) {
            e.chasing = true;
        }
    }
}
