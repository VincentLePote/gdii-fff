﻿using UnityEngine;

public class CameraScript : MonoBehaviour {

	public Transform FollowTarget;
	public Vector3 NormalTargetOffset;
	public Vector3 ZoomedTargetOffset;
	public Vector3 TargetOffset;
    

	public float MoveSpeed = 2f;
	
	private Transform _myTransform;
    public static GameObject reference;

    private void Awake() {
        reference = this.gameObject;
    }
	private void Start () {
		// Cache camera transform
		_myTransform = transform;	
	}

    public void ZoomIn() {
        if (TargetOffset != ZoomedTargetOffset)
            TargetOffset = ZoomedTargetOffset;
    }

    public void ZoomOut() {
        if (TargetOffset != NormalTargetOffset)
            TargetOffset = NormalTargetOffset;
    }
	
	public void SetTarget ( Transform aTransform ) {
		FollowTarget = aTransform;	
	}

	private void LateUpdate () {
		if(FollowTarget != null)
			_myTransform.position = Vector3.Lerp( _myTransform.position, FollowTarget.position + TargetOffset, MoveSpeed * Time.deltaTime );
	}

}
