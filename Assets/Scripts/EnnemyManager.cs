﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnnemyManager : MonoBehaviour
{
    [SerializeField]
    public EnnemyScript[] ennemies;
    public static GameObject reference;

    private void Awake() {
        reference = this.gameObject;
    }

    public void ResetAllEnnemies() {
        foreach (EnnemyScript e in ennemies) {
            if (e.chasing) {
                e.ResetPosition();
            }
        }
    }
}
