﻿using UnityEngine;
using UnityEngine.AI;

public class EnnemyScript : MonoBehaviour
{
    private NavMeshAgent nav;
    private GameObject playerRef;
    private Vector3 pos;
    public bool chasing = false;
    void Start()
    {
        nav = GetComponent<NavMeshAgent>();
        playerRef = PlayerScript.reference;
        pos = transform.position;
    }

    public void ResetPosition() {
        Debug.Log("jme reset");
        chasing = false;
        nav.enabled = false;
        transform.position = pos;
        nav.enabled = true;
    }

    void Update()
    {
        if (chasing) {
            nav.destination = playerRef.transform.position;
        }
    }
}
